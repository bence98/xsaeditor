package csokicraft.fpga.xsaeditor;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.*;
import java.util.Iterator;
import java.util.NoSuchElementException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

public class MainXSAEditor{
	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException, TransformerFactoryConfigurationError, TransformerException{
		if(args.length!=1){
			System.err.println("Usage: xsaeditor <XSA file>");
			System.exit(1);
		}
		
		Path xsaPath=Paths.get(args[0]);
		printf("Opening '%s'...", xsaPath);
		DocumentBuilder parser=DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Transformer tf=TransformerFactory.newInstance().newTransformer();
		
		try(FileSystem fs=FileSystems.newFileSystem(xsaPath, null)){
			var hwhPath=fs.getPath("/top.hwh");
			printf("Parsing XML '%s'...", hwhPath);
			
			Document hwhDoc;
			try(InputStream hwhIn=Files.newInputStream(hwhPath)){
				hwhDoc=parser.parse(hwhIn);
			}
			var modules=hwhDoc.getElementsByTagName("MODULES").item(0);
			
			var mod=getModule(modules, "VexRiscv");
			mod.getAttributes().getNamedItem("IPTYPE").setNodeValue("PROCESSOR");
			mod.getAttributes().getNamedItem("MODCLASS").setNodeValue("PROCESSOR");
			var param=getModuleParameter(mod, "EDK_IPTYPE");
			param.getAttributes().getNamedItem("VALUE").setNodeValue("PROCESSOR");
			
			DOMSource src=new DOMSource(hwhDoc);
			try(OutputStream hwhOut=Files.newOutputStream(hwhPath)){
				StreamResult res=new StreamResult(hwhOut);
				tf.transform(src, res);
			}
		}
	}

	public static Node getModule(Node modules, String modName){
		printf("Searching for module '%s'...", modName);
		for(var mod:iter(modules.getChildNodes())){
			if(!mod.getNodeName().equals("MODULE"))
				continue; // stupid text elements...
			
			if(mod.getAttributes().getNamedItem("MODTYPE").getNodeValue().equals(modName)){
				printf("Found '%s'!", modName);
				return mod;
				
				// only one VexRiscv in design
			}
		}
		return null;
	}

	public static Node getModuleParameter(Node mod, String paramName){
		for(var el:iter(mod.getChildNodes())){
			if(el.getNodeName().equals("PARAMETERS")){
				for(var param:iter(el.getChildNodes())){
					if(!param.getNodeName().equals("PARAMETER"))
						continue; // seriously...
					
					if(param.getAttributes().getNamedItem("NAME").getNodeValue().equals(paramName)){
						return param;
					}
				}
				break;
			}
		}
		return null;
	}

	private static Iterable<Node> iter(final NodeList nodeList){
		return () -> new Iterator<Node>(){
			private int i=0;

			@Override
			public boolean hasNext(){
				return i<nodeList.getLength();
			}

			@Override
			public Node next(){
				if(!hasNext()) throw new NoSuchElementException();
				return nodeList.item(i++);
			}
		};
	}
	
	private static void printf(String fmt, Object... args){
		System.out.println(String.format(fmt, args));
	}
}
